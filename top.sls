base:
  '*':
    - common.init
  '*rundeck*':
    - docker.init
  '*proxy*':
    - nginx.utils.ssl_cert
    - nginx
    - nginx.utils.nginx_policy
  '*db*':
    - postgresql.utils.swap_on
    - postgresql.utils.mount_datadisk
    - postgresql.utils.rewrite_pg_service
    - postgresql.utils.create_users
    - postgresql.utils.create_dirs
    - postgresql.utils.upstream
    - postgresql
    - postgresql.utils.add_libs
    - postgresql.utils.create_db
    - postgresql.utils.create_db_users
  '*bastion*':
    - bastion.init

dev:

  'G@environment:dev and *db*':
    - match: compound
    - postgresql.utils.first_cfg_scripts
    - postgresql.utils.pg_rman

  'G@environment:dev and *rundeck*':
    - match: compound
    - docker.utils.install_docker_compose
    - rundeck.utils.mount_containers_data
    - rundeck.init

prod:

  'G@environment:prod and *db*':
    - match: compound
    - postgresql.utils.first_cfg_scripts
    - postgresql.utils.pg_rman

  'G@environment:prod and *rundeck*':
    - match: compound
    - rundeck.init
