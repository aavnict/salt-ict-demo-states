# This state for upgrading from rundeck 2.10 to 3.0

{% set rundeck_upgrade_version= salt['pillar.get']('rundeck:rundeck_image:upgrade_version','jordan/rundeck:3.0.19') %}

pull-rundeck-image:
  docker_image.present:
    - name: {{ rundeck_upgrade_version }}
    - force: True

running-rundeck-container:
  docker_container.running:
    - name: rundeck
    - image: {{ rundeck_upgrade_version }}
    - force: True
    - binds:
      - "/opt/containers/var/rundeck:/var/rundeck"
      - "/opt/containers/var/log/rundeck:/var/log/rundeck"
      - "/opt/containers/etc/rundeck:/etc/rundeck"
      - "/opt/containers/var/lib/rundeck/ssh:/var/lib/rundeck/.ssh"
      - "/opt/containers/var/lib/rundeck/logs:/var/lib/rundeck/logs"
      - "/opt/containers/opt/rundeck-plugins:/opt/rundeck-plugins"
    - restart_policy: always
    - port_bindings:
      - "8080:4440"
    - links:
      - rundeck-db:rundeck-db
    - environment:
      - RDECK_JVM_SETTINGS: "-Xmx4096m -Xms1024m -XX:MaxMetaspaceSize=1024m -server -Dfile.encoding=UTF-8"
      - RUNDECK_STORAGE_PROVIDER: db
      - RUNDECK_PROJECT_STORAGE_TYPE: db
      - RUNDECK_PASSWORD: "{{ pillar['rundeck_gpg']['db_image']['user_password'] }}"
      - DATABASE_ADMIN_USER: root
      - DATABASE_ADMIN_PASSWORD: "{{ pillar['rundeck_gpg']['db_image']['root_password'] }}"
      - DATABASE_URL: "jdbc:mysql://rundeck-db/rundeckdb?autoReconnect=true"
      - NO_LOCAL_MYSQL: true
      - EXTERNAL_SERVER_URL: "https://{{ pillar['rundeck']['rundeck_image']['external_server_url'] }}"
      - LOGIN_MODULE: "{{ pillar['rundeck']['rundeck_image']['login_module'] }}"
      - TZ: "Europe/Zurich"
      - RUNDECK_ADMIN_PASSWORD: "{{ pillar['rundeck_gpg']['rundeck_image']['rundeck_admin_password'] }}"
      - SKIP_DATABASE_SETUP: true
    - require:
      - docker_image: pull-rundeck-image

jas_login:
  file.line:
    - name: "/opt/containers/etc/rundeck/profile"
    - content: 'JAAS_LOGIN="${JAAS_LOGIN:-true}"'
    - mode: insert
    - after: 'RDECK_HTTPS_PORT=${RDECK_HTTPS_PORT:-4443}'

{% set rdk_jvm_content = 'RDECK_JVM="-Drundeck.jaaslogin=$JAAS_LOGIN \\' | regex_escape %}
{% set rdk_jvm_match = 'RDECK_JVM="${RDECK_JVM} -Djava.security.auth.login.config=$JAAS_CONF \\' | regex_escape %}
rdeck_jvm:
  file.line:
    - name: "/opt/containers/etc/rundeck/profile"
    - content: {{ rdk_jvm_content }}
    - mode: replace
    - match: {{ rdk_jvm_match }}

{% set rdeck_base_content = '           -Drdeck.base=$RDECK_BASE \\' | regex_escape %}
rdeck_base:
  file.line:
    - name: "/opt/containers/etc/rundeck/profile"
    - content: {{ rdeck_base_content }}
    - mode: insert
    - before: '           -Dserver.http.port=$RDECK_HTTP_PORT"'

rundeckd:
  file.line:
    - name: "/opt/containers/etc/rundeck/profile"
    - content: 'rundeckd="$JAVA_CMD $RDECK_JVM $RDECK_JVM_OPTS -jar $EXECUTABLE_WAR --skipinstall"'
    - mode: replace
    - match: 'rundeckd="$JAVA_CMD $RDECK_JVM $RDECK_JVM_OPTS -cp $BOOTSTRAP_CP com.dtolabs.rundeck.RunServer $RDECK_BASE"'

war_block:
  file.line:
    - name: "/opt/containers/etc/rundeck/profile"
    - content: "for war in $(find $RDECK_INSTALL/bootstrap -name '*.war') ; do"
    - mode: replace
    - match: "for jar in $(find $RDECK_INSTALL/bootstrap -name '*.jar') ; do"

bootstrap:
  file.line:
    - name: "/opt/containers/etc/rundeck/profile"
    - content: '  EXECUTABLE_WAR=$war'
    - mode: replace
    - match: '  BOOTSTRAP_CP=${BOOTSTRAP_CP:+$BOOTSTRAP_CP:}$jar'

remove_driver_class_name:
  file.line:
    - name: /opt/containers/etc/rundeck/rundeck-config.properties
    - match: 'dataSource.driverClassName = com.mysql.jdbc.Driver'
    - mode: delete

driver_class_name:
  file.line:
    - name: /opt/containers/etc/rundeck/rundeck-config.properties
    - content: 'dataSource.driverClassName = com.mysql.jdbc.Driver'
    - mode: insert
    - before: '^dataSource.url'
    - require:
      - file: remove_driver_class_name


log4j_config:
  file.append:
    - name: /opt/containers/etc/rundeck/rundeck-config.properties
    - text:
      - 'rundeck.log4j.config.file = /etc/rundeck/log4j.properties'

log4j_logger:
  file.line:
    - name: /opt/containers/etc/rundeck/log4j.properties
    - match: 'log4j.logger.h2database = off'
    - mode: delete