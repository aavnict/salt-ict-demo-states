{% if pillar['rundeck']['disk']['device_id'] is defined %}
{% set partition_file_system = '/dev/' ~ pillar['rundeck']['disk']['device_id'] ~ '1' %}
{% else %}
{% set partition_file_system = '/dev/sdb1' %}
{% endif %}

{% set uuid = 'UUID=' ~ salt['cmd.run']('blkid -s UUID -o value ' ~ partition_file_system) %}

{% if salt['partition.exists']('' ~ partition_file_system ~ '') == False %}
parted:
  pkg.installed

mklabel_partition:
  module.run:
    - name: partition.mklabel
    - device: "/dev/{{ pillar['rundeck']['disk']['device_id'] }}"
    - label_type: msdos
    - require:
      - pkg: parted

creating_partition:
  module.run:
   - name: partition.mkpart
   - device: "/dev/{{ pillar['rundeck']['disk']['device_id'] }}"
   - start: '0%'
   - end: '100%'
   - part_type: primary
   - require:
      - module: mklabel_partition
      - pkg: parted

format_partition_fs:
  blockdev.formatted:
   - name: {{ partition_file_system }}
   - fs_type: ext4
   - force: True
   - require:
      - module: creating_partition
{% endif %}

{% for device in ['' ~ partition_file_system ~ '', '' ~ uuid ~ ''] %}
unmounted_{{ device }}:
  mount.unmounted:
   - name: "/{{ pillar['rundeck']['disk']['partition'] | default('opt/containers') }}"
   - device: {{ device }}
   - config: /etc/fstab

verify_no_mounted_{{ device }}:
  file.line:
    - name: /etc/fstab
    - match: "^{{ device }}"
    - mode: delete

{% endfor %}

mount_partion:
  mount.mounted:
   - name: "/{{ pillar['rundeck']['disk']['partition'] | default('opt/containers') }}"
   - device: {{ partition_file_system }}
   - fstype: ext4
   - config: /etc/fstab
   - mkmnt: True
   - opts:
      - defaults
      - nofail