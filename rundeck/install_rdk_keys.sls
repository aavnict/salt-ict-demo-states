{% set key_path = '/opt/containers/var/lib/rundeck/ssh' %}

rundeck_public_key:
  file.managed:
    - name: "{{ key_path }}/id_rsa.pub"
    - mode: 0600
    - makedirs: True
    - contents_pillar: 'rundeck:rundeck_image:id_rsa.pub'
  #  - show_changes: False

rundeck_private_key:
  file.managed:
    - name: "{{ key_path }}/id_rsa"
    - mode: 0600
    - makedirs: True
    - contents_pillar: 'rundeck_gpg:rundeck_image:id_rsa'
    - show_changes: False

rundeck_key_set_owner:
  cmd.run:
    #- name: "key_uid=$(stat -c '%u' {{ key_path }}) && key_gid=$(stat -c '%g' {{ key_path }}) && chown ${key_uid}:${key_gid} {{ key_path }}/*"
     - name: "chown --reference={{ key_path }} {{ key_path }}/*"
