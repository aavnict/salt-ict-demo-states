{% if salt['pillar.get']('postgres:version') == 9.6 %}

pg_rman_pkg:
  pkg.installed:
    - sources:
      - pg_rman: salt://postgresql/files/pg_rman-1.3.5-1.pg96.el7.centos.x86_64.rpm

symlink_pg_ram_binary:
  file.symlink:
    - name:  /bin/pg_rman
    - target:  /usr/pgsql-9.6/bin/pg_rman
    - require:
      - pkg: pg_rman_pkg

create pg_rman directory:
  file.directory:
    - name: /pglogs/backup/pg_rman_backup_catalog
    - user: postgres
    - group: postgres
    - mode: 700
    - makedirs: True
    - recurse:
      - user
      - group

{% endif %}
