
create user postgres:
  user.present:
    - name: postgres
{% if salt['pillar.get']('postgres:postgres_admin_password') is defined %}
    - password: {{ salt['shadow.gen_password']('' ~ pillar['postgres']['postgres_admin_password'] ~ '') }}
{% endif %}
