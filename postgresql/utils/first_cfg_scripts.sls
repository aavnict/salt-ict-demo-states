/pglogs/script/sc_axidpp_generic_create_user_admin.sql:
  file.managed:
    - source:
      - salt://postgresql/files/sc_axidpp_generic_create_user_admin.sql
    - user: postgres
    - group: postgres
    - mode: 644

/pglogs/script/sc_axidpp_generic_create_tablespace.sql:
  file.managed:
    - source:
      - salt://postgresql/files/sc_axidpp_generic_create_tablespace.sql
    - user: postgres
    - group: postgres
    - mode: 644

/pglogs/script/sc_axidpp_generic_drop_schema_public.sql:
  file.managed:
    - source:
      - salt://postgresql/files/sc_axidpp_generic_drop_schema_public.sql
    - user: postgres
    - group: postgres
    - mode: 644

/pglogs/script/sc_axidpp_generic_create_gis.sql:
  file.managed:
    - source:
      - salt://postgresql/files/sc_axidpp_generic_create_gis.sql
    - user: postgres
    - group: postgres
    - mode: 644

/pglogs/script/sc_axidpp_generic_grant_gis.sql:
  file.managed:
    - source:
      - salt://postgresql/files/sc_axidpp_generic_grant_gis.sql
    - user: postgres
    - group: postgres
    - mode: 644

sh_axidpp_generic_init:
  cmd.script:
    - name: salt://postgresql/files/sh_axidpp_generic_init.sh
    - runas: postgres
    - require:
      - file: /pglogs/script/sc_axidpp_generic_create_user_admin.sql
      - file: /pglogs/script/sc_axidpp_generic_create_tablespace.sql
      - file: /pglogs/script/sc_axidpp_generic_drop_schema_public.sql
      - file: /pglogs/script/sc_axidpp_generic_create_gis.sql
      - file: /pglogs/script/sc_axidpp_generic_grant_gis.sql

# sc_axidpp_generic_create_user_admin:
#   cmd.run:
#     - name: pgsql -p 5432 -a -f /tmp/sc_axidpp_generic_create_user_admin.sql
#     - require:
#       - file: /tmp/sc_axidpp_generic_create_user_admin.sql
#
# sc_axidpp_generic_create_tablespace:
#   cmd.run:
#     - name: pgsql -p 5432 -a -f /tmp/sc_axidpp_generic_create_tablespace.sql
#     - require:
#       - file: /tmp/sc_axidpp_generic_create_tablespace.sql
#
# sc_axidpp_generic_drop_schema_public:
#   cmd.run:
#     - name: pgsql -p 5432 -a -f /tmp/sc_axidpp_generic_drop_schema_public.sql
#     - require:
#       - file: /tmp/sc_axidpp_generic_drop_schema_public.sql
#
# sc_axidpp_generic_create_gis:
#   cmd.run:
#     - name: pgsql -p 5432 -a -f /tmp/sc_axidpp_generic_create_gis.sql
#     - require:
#       - file: /tmp/sc_axidpp_generic_create_gis.sql
#
# sc_axidpp_generic_grant_gis:
#   cmd.run:
#     - name: pgsql -p 5432 -a -f /tmp/sc_axidpp_generic_grant_gis.sql
#     - require:
#       - file: /tmp/sc_axidpp_generic_grant_gis.sql
