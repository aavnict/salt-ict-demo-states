{% set version = salt['pillar.get']('postgres:version') %}
{% set release = version | replace('.', '') %}
{% set pg_lib = [
   { 'src': 'file.so_pg' ~ release, 'dest': '/usr/pgsql-' ~ version ~'/lib/file.so' },
   { 'src': 'lib_postgresqludf_sys.so_pg' ~ release, 'dest': '/usr/pgsql-' ~ version ~ '/lib/lib_postgresqludf_sys.so' }
]
%}
{% for lib in pg_lib %}
add pg_lib {{lib.dest}}:
 file.managed:
   - name: {{ lib.dest }}
   - source: salt://postgresql/files/{{ lib.src}}
   - user: root
   - group: root
   - mode: 755
{% endfor %}
