{% from "postgresql/macros.jinja" import format_kwargs with context %}
{% for user, args in salt['pillar.get']('postgres:users').iteritems() %}
create postgres user {{ user }}:
  postgres_user.present:
    - name: {{ user }}
  {{- format_kwargs(args) }}
{% endfor %}
