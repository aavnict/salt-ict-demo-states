{% set postgres_user = "postgres" %}
{% for dir in salt['pillar.get']('postgres:new_pg_dirs') %}
create folder {{ dir }}:
  file.directory:
    - name: {{ dir }}
    - user: {{ postgres_user }}
    - group: {{ postgres_user }}
    - mode: 0700
    - makedirs: True
{% endfor %}

{% for symlink in salt['pillar.get']('postgres:new_pg_symlinks') %}
Create postgres {{ symlink.name }} symlink:
  file.symlink:
    - name: {{ symlink.name }}
    - target: {{ symlink.target }}
    - user: {{ postgres_user }}
    - group: {{ postgres_user }}
    - makedirs: True
    - force: True
{% endfor %}
