CREATE TABLESPACE data OWNER postgres LOCATION '/pgdata/data';
REVOKE ALL ON TABLESPACE data FROM PUBLIC;
REVOKE ALL ON TABLESPACE data FROM postgres;
GRANT ALL ON TABLESPACE data TO postgres;

CREATE TABLESPACE index OWNER postgres LOCATION '/pgindex/index';
REVOKE ALL ON TABLESPACE index FROM PUBLIC;
REVOKE ALL ON TABLESPACE index FROM postgres;
GRANT ALL ON TABLESPACE index TO postgres;

CREATE TABLESPACE temp OWNER postgres LOCATION '/pgindex/temp';
REVOKE ALL ON TABLESPACE temp FROM PUBLIC;
REVOKE ALL ON TABLESPACE temp FROM postgres;
GRANT ALL ON TABLESPACE temp TO postgres;

set temp_tablespaces to temp;
set default_tablespace to data;
