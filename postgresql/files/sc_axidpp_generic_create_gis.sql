CREATE ROLE rol_gis_del;
ALTER ROLE rol_gis_del WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE rol_gis_exe;
ALTER ROLE rol_gis_exe WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE rol_gis_ins;
ALTER ROLE rol_gis_ins WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE rol_gis_sel;
ALTER ROLE rol_gis_sel WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE rol_gis_upd;
ALTER ROLE rol_gis_upd WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;

CREATE SCHEMA gis
  AUTHORIZATION postgres;

GRANT ALL ON SCHEMA gis TO postgres;
GRANT USAGE ON SCHEMA gis TO rol_gis_sel;
GRANT USAGE ON SCHEMA gis TO rol_gis_ins;
GRANT USAGE ON SCHEMA gis TO rol_gis_upd;
GRANT USAGE ON SCHEMA gis TO rol_gis_del;
GRANT USAGE ON SCHEMA gis TO rol_gis_exe;
COMMENT ON SCHEMA gis
  IS 'GIS functionality';

CREATE EXTENSION postgis SCHEMA gis;
