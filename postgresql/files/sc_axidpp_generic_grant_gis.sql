SELECT 'REVOKE ALL ON TABLE gis.'||tablename||' FROM public;' from pg_tables where schemaname='gis';
SELECT 'GRANT SELECT ON TABLE gis.'||tablename||' TO rol_gis_sel;' from pg_tables where schemaname='gis';
SELECT 'GRANT INSERT ON TABLE gis.'||tablename||' TO rol_gis_ins;' from pg_tables where schemaname='gis';
SELECT 'GRANT UPDATE ON TABLE gis.'||tablename||' TO rol_gis_upd;' from pg_tables where schemaname='gis';
SELECT 'GRANT DELETE ON TABLE gis.'||tablename||' TO rol_gis_del;' from pg_tables where schemaname='gis';
SELECT 'REVOKE ALL ON TABLE gis.'||viewname||' FROM public;' from pg_views where schemaname='gis';
SELECT 'GRANT SELECT ON TABLE gis.'||viewname||' TO rol_gis_sel;' from pg_views where schemaname='gis';
SELECT 'REVOKE EXECUTE ON FUNCTION '|| n.nspname || '.' || p.proname || '(' || pg_catalog.oidvectortypes(p.proargtypes) || ') '||'from public;' FROM pg_proc p LEFT JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace where n.nspname='gis';
SELECT 'GRANT EXECUTE ON FUNCTION '|| n.nspname || '.' || p.proname || '(' || pg_catalog.oidvectortypes(p.proargtypes) || ') '||'TO rol_gis_exe;' FROM pg_proc p LEFT JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace where n.nspname='gis';