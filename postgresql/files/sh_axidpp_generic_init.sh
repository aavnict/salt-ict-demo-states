#!/usr/bin/env bash

function _log_() {
    LEVEL=$1
    shift
    echo "$(date "+[%Y/%m/%d %T %z]") [$LEVEL] $*"
}

function log_i() {
    _log_ INFO "$@"
}

function log_e() {
    _log_ ERROR "$@"
}

function log_w() {
    _log_ WARNING "$@"
}

# Directory storing scripts
DIR=/pglogs/script

# Being read as environment variable
#PG_DB=
#PG_PORT=
# creates login role admin with no password -> has to be set manually
SCRIPT="sc_axidpp_generic_create_user_admin.sql"
log_i "Executing: $SCRIPT"
/usr/bin/psql -p "$PG_PORT" -U postgres -t -A --set ON_ERROR_STOP=on -q -f $DIR/$SCRIPT $PG_DB
PSQL_EXIT_STATUS=$?
if [ $PSQL_EXIT_STATUS != 0 ]; then
    log_e "psql failed while trying to run the script $SCRIPT"
    exit $PSQL_EXIT_STATUS
fi

# creates tablespaces with correct owner and initial grants. Furthermore sets config values for "temp_tablespaces" and "default_tablespace"
SCRIPT="sc_axidpp_generic_create_tablespace.sql"
log_i "Executing: $SCRIPT"
/usr/bin/psql -p "$PG_PORT" -U postgres -t -A --set ON_ERROR_STOP=on -q -f $DIR/$SCRIPT $PG_DB
PSQL_EXIT_STATUS=$?
if [ $PSQL_EXIT_STATUS != 0 ]; then
    log_e "psql failed while trying to run the script $SCRIPT"
    exit $PSQL_EXIT_STATUS
fi

# drops public schema
SCRIPT="sc_axidpp_generic_drop_schema_public.sql"
log_i "Executing: $SCRIPT"
/usr/bin/psql -p "$PG_PORT" -U postgres -t -A --single-transaction --set ON_ERROR_STOP=on -q -f $DIR/$SCRIPT $PG_DB
PSQL_EXIT_STATUS=$?
if [ $PSQL_EXIT_STATUS != 0 ]; then
    log_e "psql failed while trying to run the script $SCRIPT"
    exit $PSQL_EXIT_STATUS
fi

# creates schema gis, installs extension gis in schema gis and sets schema grants for roles
SCRIPT="sc_axidpp_generic_create_gis.sql"
log_i "Executing: $SCRIPT"
/usr/bin/psql -p "$PG_PORT" -U postgres -t -A --single-transaction --set ON_ERROR_STOP=on -q -f $DIR/$SCRIPT $PG_DB
PSQL_EXIT_STATUS=$?
if [ $PSQL_EXIT_STATUS != 0 ]; then
    log_e "psql failed while trying to run the script $SCRIPT"
    exit $PSQL_EXIT_STATUS
fi

# sets correct grants for all objects in schema gis (dynamic)
SCRIPT="sc_axidpp_generic_grant_gis.sql"
log_i "Executing: $SCRIPT"
PSQL_EXIT_STATUS=""
/usr/bin/psql -p "$PG_PORT" -U postgres -t -A --single-transaction --set ON_ERROR_STOP=on -q -f $DIR/$SCRIPT | /usr/bin/psql -p "$PG_PORT" -U postgres $PG_DB
PSQL_EXIT_STATUS=$?
if [ $PSQL_EXIT_STATUS != 0 ]; then
    log_e "psql failed while trying to run the script $SCRIPT"
    exit $PSQL_EXIT_STATUS
fi


log_i "All sql scripts where successfully executed"
exit 0
