{% if salt['file.file_exists']('' ~ pillar['docker']['docker_compose']['binary_path'] ~'/docker-compose') == True %}
{% set docker_compose_current_version = salt['cmd.run']('docker-compose --version') %}
{% if pillar['docker']['docker_compose']['version'] not in docker_compose_current_version %}
clean_docker_compose:
  file.absent:
    - name: {{ pillar['docker']['docker_compose']['binary_path'] }}
{% endif %}
{% endif %}

installing_docker_compose:
  file.managed:
    - name: {{ pillar['docker']['docker_compose']['binary_path'] }}
    - source: https://github.com/docker/compose/releases/download/{{ pillar['docker']['docker_compose']['version'] }}/docker-compose-Linux-x86_64
    - skip_verify: True
    - mode: 0755