{% if 'dev' in data.id %}
{% set env = 'dev' %}
{% elif 'prod' in data.id %}
{% set env = 'prod' %}
{% else %}
{% set env = 'none' %}
{% endif %}

set_environment:
  local.grains.setval:
    - tgt: {{ data['id'] }}
    - arg:
      - environment
      - {{ env }}
