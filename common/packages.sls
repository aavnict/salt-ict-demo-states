
{% set pkgs = salt['pillar.get']('common:pkgs:basic') + salt['pillar.get']('common:pkgs:utils')%}
updating all packages:
  pkg.uptodate:
    - refresh: True

install packages:
  pkg.installed:
    - pkgs: {{ pkgs }}
