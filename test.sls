{% for user in pillar['bastion_gpg']['gg_2fa'] %}
{% set text = salt['pillar.get']('bastion:gg_2fa:' ~ user ~ '') %}
{{ user }}:
   module.run:
    - name: test.echo
    - text: {{ text }}
{% endfor %}