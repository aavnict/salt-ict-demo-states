creating_forwarding_group:
  group.present:
    - name: "{{ pillar['bastion']['config']['groups']['name'] }}"

adding_cloud_user_to_group:
  user.present:
    - name: cloud-user
    - groups:
      - "{{ pillar['bastion']['config']['groups']['name'] }}"
    - remove_groups: False
    - require:
      - group: creating_forwarding_group

creating_bastion_log_dir:
  file.directory:
    - name: "{{ pillar['bastion']['config']['log_dir'] }}"
    - user: cloud-user
    - group: {{ pillar['bastion']['config']['groups']['name'] }}
    - dir_mode: 770
    - file_mode: 770
    - recurse:
      - user
      - group
      - mode
    - require:
      - group: creating_forwarding_group

creating_action_log_dir:
  file.directory:
    - name: "{{ pillar['bastion']['config']['log_dir'] }}action_log"
    - user: cloud-user
    - group: {{ pillar['bastion']['config']['groups']['name'] }}
    - dir_mode: 770
    - file_mode: 770
    - recurse:
      - user
      - group
      - mode
    - require:
        - group: creating_forwarding_group
        - file: creating_bastion_log_dir

creating_access_log_dir:
  file.directory:
    - name: "{{ pillar['bastion']['config']['log_dir'] }}access_log"
    - user: cloud-user
    - group: {{ pillar['bastion']['config']['groups']['name'] }}
    - dir_mode: 770
    - file_mode: 770
    - recurse:
      - user
      - group
      - mode
    - require:
        - group: creating_forwarding_group
        - file: creating_bastion_log_dir

creating_bastion_acl:
  cmd.run:
    - name: setfacl -Rdm other:0 /var/log/bastion
    - require:
      - file: creating_bastion_log_dir

/usr/bin/bastion/shell:
   file.managed:
    - source: 'salt://bastion/files/shell.jinja'
    - template: jinja
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

backup_sshd_file:
  cmd.run:
    - name: cp -r /etc/ssh/sshd_config /etc/ssh/sshd_config.bak

sshd_configuration:
  file.managed:
    - name: /etc/ssh/sshd_config
    - template: jinja
    - source: "salt://bastion/files/sshd_config.jinja"
    - user: root
    - group: root
    - mode: 600
    - require:
      - cmd: backup_sshd_file

# Block to allow changing default ssh port
{% set ssh_port = salt['pillar.get']('bastion:config:ssh_port','22') %}
{% if ssh_port != '22' %}
policycoreutils-python:
  pkg.installed:
    - name: policycoreutils-python
    - require:
      - file: sshd_configuration

selinux_allow_ssh_port:
  cmd.run:
    - name: semanage port -a -t ssh_port_t -p tcp {{ ssh_port }} || echo "Port already defined"
    - require:
      - pkg: policycoreutils-python
      - file: sshd_configuration
{% endif %}
#########
mount_hide_id:
  cmd.run:
    - name: mount -o remount,rw,hidepid=2 /proc

remove_proc:
  file.line:
    - name: /etc/fstab
    - match: '^proc'
    - mode: delete

owner_proc:
  file.append:
    - name: /etc/fstab
    - text:
      - 'proc /proc proc defaults,hidepid=2 0 0'
    - require:
      - file: remove_proc

sshd_running:
  service.running:
    - name: sshd
    - enable: True
    - reload: True
    - watch:
      - file: sshd_configuration