installing_gg_2fa:
  pkg.installed:
    - name: google-authenticator

config_pam_sshd_file:
  file.managed:
    - name: /etc/pam.d/sshd
    - source: 'salt://bastion/files/gg-2fa/sshd'
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: installing_gg_2fa

google_authenticator_for_cloud_user:
  file.managed:
    - name: "/home/cloud-user/.google_authenticator"
    - user: cloud-user
    - group: cloud-user
    - mode: 400
    - makedirs: True
    - contents_pillar: 'bastion_gpg:gg_2fa:cloud_user'
    - show_changes: False
    - require:
      - pkg: installing_gg_2fa