sync_users:
  file.managed:
    - name: /usr/bin/bastion/sync_users
    - source: 'salt://bastion/files/sync_users.jinja'
    - template: jinja
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

cron_sync_users:
  cron.present:
    - user: root
    - name: '/usr/bin/bastion/sync_users'
    - minute: '*/5'
    - hour: '*'
    - daymonth: '*'
    - month: '*'
    - dayweek: '*'
    - identifier: 'cron-sync-users'
    - comment: 'Managed by salt-stack'
    - require:
      - file: /usr/bin/bastion/sync_users

sync_logs:
  file.managed:
    - name: /usr/bin/bastion/sync_logs
    - source: 'salt://bastion/files/sync_logs.jinja'
    - template: jinja
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

cron_get_faillock_log:
  cron.present:
    - name: "faillock > {{ pillar['bastion']['config']['log_dir'] }}faillock.log"
    - user: root
    - minute: '*/4'
    - hour: '*'
    - daymonth: '*'
    - month: '*'
    - dayweek: '*'
    - identifier: 'cron_get_faillock_log'
    - comment: 'Managed by salt-stack'

cron_sync_logs:
  cron.present:
    - user: root
    - name: '/usr/bin/bastion/sync_logs'
    - minute: '*/5'
    - hour: '*'
    - daymonth: '*'
    - month: '*'
    - dayweek: '*'
    - identifier: 'cron-sync-logs'
    - comment: 'Managed by salt-stack'
    - require:
      - file: sync_logs

cron-security-updates:
  cron.present:
    - user: root
    - name: 'yum -y update --security'
    - minute: '0'
    - hour: '0'
    - daymonth: '*'
    - month: '*'
    - dayweek: '*'
    - identifier: 'cron-security-updates'
    - comment: 'Managed by salt-stack'
