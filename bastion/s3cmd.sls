installing_s3cmd:
  pkg.installed:
    - name: s3cmd

s3cmd_configure:
  file.managed:
    - name: /root/.s3cfg
    - source: "salt://bastion/files/s3cfg.jinja"
    - template: jinja
    - user: root
    - group: root
    - mode: 700
    - show_changes: False