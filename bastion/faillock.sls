faillock_system_auth:
  file.managed:
    - name: /etc/pam.d/system-auth-ac
    - source: 'salt://bastion/files/faillock.pam/system-auth-ac.jinja'
    - template: jinja
    - user: root
    - groups: root
    - mode: 644

faillock_password_auth:
  file.managed:
    - name: /etc/pam.d/password-auth-ac
    - source: 'salt://bastion/files/faillock.pam/password-auth-ac.jinja'
    - template: jinja
    - user: root
    - groups: root
    - mode: 644

sshd:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: faillock_system_auth
      - file: faillock_password_auth