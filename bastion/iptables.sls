stop_firewalld:
  service.dead:
    - name: firewalld
    - enable: False

iptables_services:
  pkg.installed:
    - name: iptables-services

iptables_enable:
  service.running:
    - name: iptables
    - enable: True
    - require:
      - pkg: iptables_services

iptables_restart:
  file.replace:
    - name: /etc/sysconfig/iptables-config
    - pattern: ^IPTABLES_SAVE_ON_RESTART=.*
    - repl: "IPTABLES_SAVE_ON_RESTART=\"yes\""
    - require:
      - pkg: iptables_services

iptables_stop:
  file.replace:
    - name: /etc/sysconfig/iptables-config
    - pattern: ^IPTABLES_SAVE_ON_STOP=.*
    - repl: "IPTABLES_SAVE_ON_STOP=\"yes\""
    - require:
      - pkg: iptables_services

iptable_startup:
  cmd.run:
    - name: service iptables save; chkconfig iptables on
    - require:
      - pkg: iptables_services