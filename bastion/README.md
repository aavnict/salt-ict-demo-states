### States description
- Configure sshd.
- SSH Authentication by key and Google 2FA.
- Manage SSH public keys and 2FA keys in S3 and automatic creating users.
- Restrict user access to destination host by access list in S3 and automatic configure iptables.
- Configure the security of processes for each user.
- Action log users management.

- Pillar file in another repositories:
      - https://bitbucket.org/aavnict/salt-pillar-practices/bastion
      - https://bitbucket.org/aavnict/salt-ict-demo-secrets/bastion

### Requirement:
- Install common states first.

### Example for S3 structure

```
bastion
 |-- logs
 |-- users
    |----- tvtin.pub # user public key
    |----- tvtin.2fa # user google authenticator file.
    |----- ttdung.pub
    |----- ttdung.2fa
  |-- acls
    |----- users.acl # access list file for users
 
```

### Example for users.acl file

- Below is example for access list file.
    - cloud-user: allow to connect all servers
    - tvtin: allow to connect subnet ip 192.168.77.0/24
    - ttdung: allow to connect to 192.168.77.100 and domain web-1.aavn.local
```
[cloud-user]
*
[tvtin]
192.168.77.0/24
[ttdung]
192.168.77.100
web-1.aavn.local 
```

### More references:
- Google 2FA : https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-centos-7


